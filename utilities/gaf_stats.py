from scripts.extract_data import *
from scripts.call import *
import defopt

def main(GAF: str, PATHS: str, GENOME: str, *, outdir: str):
	"""
	Check if reads map on the correct genome
    
	:param GAF: GAF file from vg giraffe
	:param PATHS: TSV file obtained with graph_segments.py
	:param GENOME: Provenance genome of reads
	"""
    
	gaf_df = get_info_gaf(GAF)
	# get read start and end positions
	read_pos = extract_read_pos(gaf_df["Read ID"], "_")

	# get alignment orientation and first segment from path
	orient_seg = get_segment(gaf_df["Path"])
	from_gfa = get_seg_origin(orient_seg, PATHS, GENOME)

	d = { #"Read ID" : gaf_df["Read ID"], 
		"Read start": read_pos[0], "Read end": read_pos[1], 
		"Mapping quality": gaf_df["Mapping quality"], 
		"Number of matching residues": gaf_df["Number of matching residues"], 
		"Path ID": orient_seg,
		"Path origin": from_gfa,
		"Alignment information": gaf_df["Alignment information"],
		"Softclip": gaf_df["Softclip"]
		}

	df = pd.DataFrame(data=d)
	# print(df.columns)
	# print(gaf_df.head)

	##----------------------------------------------------------------------
	name = get_basename(outdir, GAF)
	write_df(df, name)
	write_figs(df, name)

	# ##----------------------------------------------------------------------


	df.to_csv(name + "_all.tsv", sep="\t")

if __name__ == '__main__':
    defopt.run(main)
