import os
from scripts.gaf_plots import *
from scripts.gaf_sum_table import *

def get_basename(newdir, input_file):
    name = os.path.basename(input_file)
    name = os.path.splitext(name)[0]
    base = newdir + "/" + name + "/" + name
    if not os.path.exists(newdir + "/" + name):
        os.makedirs(newdir + "/" + name)
    return(base)

# plots
def write_figs(df, dir):
    match_histo(df, dir)
    map_qual_histo(df, dir)
    match_per_cat_box(df, dir)
    map_qual_per_cat_box(df, dir)
    
# write all df to tsv
def write_df(df, path):
    # global stat summary
    df.describe().to_csv(path + "_global_stats.tsv", sep="\t")
    # number of matching residues statistics
    df2 = summary_per_cat_align(df)
    df2.to_csv(path + "_alignment_match.tsv", sep="\t")
    # mapping quality statistics
    df3 = summary_per_cat_mapq(df)
    df3.to_csv(path + "_alignment_mapq.tsv", sep="\t")
    # alignment type summary
    df4 = summary_per_cat(df)
    df4.to_csv(path + "_alignment_type.tsv", sep="\t")
    # softclips and alignment type
    df5 = softclip_cat_count(df)
    df5.to_csv(path + "_softclip_alignment.tsv", sep="\t")
    # # mapping quality per origin
    df6 = mapq_origin(df)
    df6.to_csv(path + "_mapq_per_origin.tsv", sep="\t")
    # match per origin
    df7 = match_origin(df)
    df7.to_csv(path + "_match_per_origin.tsv", sep="\t")
    
