#!/usr/bin/env python

def summary_table(df):
    sum_table = df.describe()
    sum_table = sum_table.to_html(table_id="global")
    return(sum_table)

# number of matching residues statistics per alignment type
def summary_per_cat_align(df):
    t = df.groupby(['Alignment information'])["Number of matching residues"].describe().reset_index()
    return(t)

# mapping quality statistics per alignment type
def summary_per_cat_mapq(df):
    t = df.groupby(['Alignment information'])["Mapping quality"].describe().reset_index()
    return(t)

# alignment type summary
def summary_per_cat(df):
    t = df.groupby(['Alignment information']).describe().reset_index()
    return(t)

# count mapq
def map_qual_count(df):
    c = df['Mapping quality'].value_counts(normalize=True, bins=2)
    return(c)

# count alignment types
def align_info_count(df):
    c = df['Alignment information'].value_counts().to_frame()
    return(c)

# softclips and alignment type
def softclip_cat_count(df):
    softclips = df[(df.Softclip == "yes")]["Alignment information"].value_counts().to_frame()
    return(softclips)

def mapq_origin(df):
    map_df = df.groupby(['Path origin'])["Mapping quality"].describe().reset_index()
    return(map_df)

def match_origin(df):
    match_df = df.groupby(['Path origin'])["Number of matching residues"].describe().reset_index()
    return(match_df)