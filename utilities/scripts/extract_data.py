#!/usr/bin/env python

import pandas as pd
import ast

##----------------------------------------------------------------------
## Extract information from GAF
def get_info_gaf(input_file):
    colnames=['Read ID', 'Path', 'Path length', 'Start', 'End', 'Number of matching residues', 'Mapping quality', 'Difference string'] 
    
    ## vérification de taille pour résoudre problème de colonnes manquantes
    with gzip.open(input_file, 'rt') as f:
        first_line = f.readline().strip('\n')
        li = first_line.split('\t')
        if len(li) > 15:
            n_col = len(li)
        else:
            n_col = 15
    f.close()
    
    df = pd.read_csv(input_file, sep='\t', usecols=[0,5,6,7,8,9,11,14], header = None, 
                     names=range(n_col),
                     dtype={'Read ID' : str, 'Path' : str,'Path length' : str, 'Start' : str, 'End' : str, 
                            'Number of matching residues' : str, 'Mapping quality' : int, 'Difference string' : str})
    df.columns = colnames

    # retire les valeurs * = reads non alignés
    # df = df.drop(df[df["Path"] == "*"].index)
    df['Number of matching residues'] = df['Number of matching residues'].replace("*", 0)
    df = df.replace("*", -1)
    # to numeric
    df["Path length"] = pd.to_numeric(df["Path length"])
    df["Start"] = pd.to_numeric(df["Start"])
    df["End"] = pd.to_numeric(df["End"])
    df["Number of matching residues"] = pd.to_numeric(df["Number of matching residues"])
     # passer la colonne en type int
    
    
    # nouvelle colonne
    df['Alignment information'] = list_mismatchs(df["Difference string"])
    # nouvelle colonne
    df['Softclip'] = list_sofclips(df["Difference string"])

    return(df)

##----------------------------------------------------------------------
# pour chaque alignement, renvoit s'il y a insertion (+), délétion (-) ou substitution (*)
def list_mismatchs(col):
    li = []
    for i in col:
        if(isinstance(i, str)):
            temp = ""
            if "*" in i:
                temp = temp + "*"
            if "+" in i:
                temp = temp + "+"
            if "-" in i:
                temp = temp + "-"
            if temp == '':
                temp = "perfect"
        else:
            temp = "not aligned"
        li.append(temp)
    return(li)

# pour chaque alignement, renvoit s'il y a des softclips (= insertion en bout de reads)
def list_sofclips(li):
    sftclip = []
    for i in li:
        if(isinstance(i, str)):
            cut = i.split("Z:")
            align = cut[1].split(":")
            if "+" in align[0] or "+" in align[-1]:
                sftclip.append("yes")
            else:
                sftclip.append("no")
        else:
            sftclip.append("no")
    return(sftclip)

# read id + "_"
def extract_read_pos(li, separator):
    read_start = []
    read_end = []
    for i in li:
        get_pos = i.split(separator)
        read_start.append(get_pos[2])
        read_end.append(get_pos[3])
    return(read_start, read_end)

# path + "s"
def get_segment(li):
    orientation = []
    segment = []
    for i in li:
        if i == -1:
            orientation.append("no alignment")
            segment.append("no alignment")
        else:
            s = i.replace("<", ">")
            s = s.strip(">") # évite les entrées vides dans la liste
            seg = s.split(">")
            segment.append(seg)
    return(segment)

##----------------------------------------------------------------------
## Extract information from GFA
# def get_gen_seg(input_file, genome):
#     df = pd.read_csv(input_file, sep='\t', header = None, names=["Genome", "Segments"])
#     gen_df = df[df["Genome"] == genome]
#     print(gen_df)
#     gen_li = ast.literal_eval(gen_df["Segments"].iloc[0])
#     print(gen_li)
#     return(gen_li)


# récupère l'origine des segments
def get_seg_origin(seg_li, tsv, genome):
    origin = []
    # flat_seg_li = flatten(seg_li)
    # unique_segment = set(flat_seg_li)
    # seg_li = list(unique_segment)

    # récupère la liste des segments appartenant à un génome
    # à partir du tsv obtenu avec les chemins d'un GFA
    df = pd.read_csv(tsv, sep='\t', header = None, names=["Genome", "Segments"])
    gen_df = df[df["Genome"] == genome]
    gen_li = ast.literal_eval(gen_df["Segments"].iloc[0])
    for i in seg_li:
        if i != "no alignment":
            if i[0] in gen_li and i[-1] in gen_li:
                res = genome
            else:
                res = "other genome"
            # for j in i:
            #     if j in gen_li:
            #         res = genome
            #         # origin.append(genome)
            #     else:
            #         res = "other genomes"
            #         # origin.append("other genomes")
            origin.append(res)
        else:
            origin.append("no alignment")
    return(origin)

# get_gen_seg("segment_per_genome/minigraph_7genomes_paths_list.tsv", "g1_chr8")

def flatten(l):
    return [item for sublist in l for item in sublist]