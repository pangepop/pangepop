import plotly.express as px

### HISTOGRAMS
def match_histo(df, dir):
    fig = px.histogram(df, x="Number of matching residues", color = "Softclip", log_y=True)
    # ajoute la moyenne sur l'histogramme
    moy = df["Number of matching residues"].mean()
    fig.add_vline(x=moy, line_width=1, line_dash="dash",
                annotation_text="Mean = " + str(round(moy,3)),
                annotation_position="top left")

    fig.write_image(dir + "_match_histogram.png")

def map_qual_histo(df, dir):
    fig = px.histogram(df, x="Mapping quality", color = "Softclip", log_y=False)
# fig.add_vline(x=30, line_width=1, line_dash="dash", line_color="grey")
    moy = df["Mapping quality"].mean()
    fig.add_vline(x=moy, line_width=1, line_dash="dash", 
              annotation_text="Mean = " + str(round(moy,3)), 
              annotation_position="top left")
    fig.write_image(dir + "_map_quality_histogram.png")

### BOXPLOTS
def match_per_cat_box(df, dir):
    df = df.sort_values("Alignment information").reset_index(drop=True)
    # boxplot de mapping quality en fonction du type d'event d'alignement
    fig = px.box(df, x="Alignment information", y="Number of matching residues", color = "Softclip")
    fig.write_image(dir + "_match_boxplot.png")

def map_qual_per_cat_box(df, dir):
    df = df.sort_values("Alignment information").reset_index(drop=True)
    # boxplot de mapping quality en fonction du type d'event d'alignement
    fig = px.box(df, x="Alignment information", y="Mapping quality", color = "Softclip")
    fig.write_image(dir + "_mapping_boxplot.png")