Extract alignment stats from GAF file obtained with vg giraffe.

# 2 steps

## 1. Extract segment lists from GFA paths 
Command
```
python graph_segments.py <TSV> -g <graph type> -m <mapping quality> -o <OUTDIR>
```

With :
- TSV file containing paths of each genomes in pangenome graph
- Pangenome graph type (possible values: rGFA, GFA1.1)
- Mapping quality cutoffs for rGFA

Le TSV est obtenu en mappant les génomes sur le graph avec minigraph dans le cas d'un rGFA construit avec minigraph
```
minigraph --vc minigraph_7genomes.gfa g2.chr8.fasta > g2.tsv
```

Ou directement dans le GFA dans le cas d'un GFA1.1 obtenu avec minigraph-cactus
```
grep "^W" minigraph_cactus_7genomes.gfa |cut -f1-6 > mc.tsv
```

## 2. Check if reads map on the correct genome
Command
```
python gaf_stats.py <GAF.gz> <TSV> <GENOME> -o <OUTDIR>
```

With
- GAF.gz file from vg giraffe
- TSV file obtained with graph_segments.py
- GENOME on which reads should align (use the same name as in TSV)