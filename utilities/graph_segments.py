#!/usr/bin/env python
import pandas as pd
import os, defopt

### script à lancer seul avant de faire l'analyse avec le GAF
### Donne un dictionnaire des segments par génome dans le graphe GFA
### Pour le moment prend du rGFA (minigraph) et du GFA1.1 (minigraph-cactus)

### python gfa_segments.py path.tsv rGFA mapping_quality(optional) 
# récupère les segments sous forme de liste à partir d'un chemin
def get_segment(li, g):
	segment = []
	if g == "rGFA":
		for i in li:
			s = i.replace(">", "").replace("<", "")
			s = s.strip("s") # évite les entrées vides dans la liste
			seg = s.split("s")
			seg = ["s" + i for i in seg]
			segment += seg
	# pas de s devant int dans l'ID
	elif g == "GFA1.1":
		for i in li:
			s = i.replace("<", ">")
			s = s.strip(">") # évite les entrées vides dans la liste
			seg = s.split(">")
			segment += seg
	unique_segment = set(segment)
	segment = list(unique_segment)
	# segment = [int(i) for i in segment]
	return segment

# dictionnaire avec ID et liste des segments correspondants
def segment_origin(input_file, graph_type, mapq, outdir):
	# df
	if graph_type == "rGFA":
		df = pd.read_csv(input_file, sep='\t', usecols=[0,5,11], header = None, 
						names=["ID", "Path", "MAPQ"])
		df = df.drop(df[df.MAPQ < mapq].index)
	elif graph_type == "GFA1.1":
		df = pd.read_csv(input_file, sep='\t', usecols=[1,6], header = None, 
						names=["ID", "Path"])

	ids = df['ID'].unique()

	# en tsv
	if not os.path.exists(outdir):
		os.makedirs(outdir)
	name = os.path.basename(input_file)
	name = os.path.splitext(name)[0]
	with open(outdir + "/" + name + "_segments.tsv", 'w') as f:
		for i in ids:
			li =df.loc[df['ID'] == i, 'Path'].tolist()
			id_seg = get_segment(li, graph_type)
			f.write(i + "\t" + str(id_seg) + "\n")
	f.close()
	return(id_seg)



def main(paths: str,   *, outdir: str, graph: str = "rGFA", mapqual: int = 20):
	"""
	Extract segment lists from GFA paths

	:param paths: TSV file containing paths of each genomes in pangenome graph
	:param graph: Pangenome graph type (possible values: rGFA, GFA1.1)
	:param mapqual: Mapping quality cutoffs for rGFA
	"""

	segment_origin(paths, graph, mapqual, outdir)

if __name__ == '__main__':
    defopt.run(main)



### ----------------------------------------------------------------------------------------------
# 1. GFA paths
# 2. GFA type : rGFA or GFA1.1
# 3. optional mapping quality cutoffs

# # filename and output
# name = os.path.basename(sys.argv[1])
# name = os.path.splitext(name)[0]

# # /home/sukanya/tests/Analyses/with_gfa/minigraph_vc_path/minigraph_7genomes_paths.tsv
# python code/gfa_segments.py /home/sukanya/tests/Analyses/with_gfa/mc_tritici_paths.tsv GFA1.1
