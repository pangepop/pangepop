def rule_reads(wildcards):
    sample = wildcards.sample
    li = config["reads"][f'{sample}']
    if len(li) > 1:
        return(li[0] + " -f " + li[1])
    else:
        return(li[0])

def check_reads(wildcards):
    sample = wildcards.sample
    li = config["reads"][f'{sample}']
    return(li)

def get_samples(sample_tsv):
    import pandas as pd
    import yaml
    
    df = pd.read_csv(sample_tsv, sep='\t', header = None, names=["sample", "read1", "read2"])

    ## reads as a list
    df = df.assign(reads = df[["read1", "read2"]].stack().groupby(level=0).agg(list))
    df = df.drop(["read1", "read2"], axis=1)
    ## samples to YAML
    dict_df = df.set_index('sample').to_dict()
    with open('workflow/config_samples.yaml', 'w+') as outfile:
        yaml.dump(dict_df, outfile, allow_unicode=True, default_flow_style=True)
    
    return(df["sample"].tolist())