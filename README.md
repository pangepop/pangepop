# Pangepop
This [Snakemake](https://snakemake.readthedocs.io/en/stable/) workflow uses [minigraph-cactus](https://github.com/ComparativeGenomicsToolkit/cactus/blob/master/doc/pangenome.md) to create a pangenome and [vg](https://github.com/vgteam/vg) to genotype short reads on a pangenome graph (with the `vg giraffe` command). 

## Table of contents
[TOC]

## Inputs & Outputs
The inputs are :
- **(seqFile)** a  file of all genomes to construct the pangenome. It is a two-column file mapping sample names to fasta paths (gzipped fastas are supported), as in [minigraph-cactus documentation](https://github.com/ComparativeGenomicsToolkit/cactus/blob/master/doc/pangenome.md#interface)
```
genome1	path/to/genome.fasta
genome2	path/to/genome.fasta
genome3	path/to/genome.fasta
```

- **(sample.tsv)** a tab-separated file with all short reads (paired or not) to genotype. It is a two or three-column (if paired reads) file mapping sample names to fasta/fastq paths (gzipped fastas are supported).
```
id1	path/to/r1.fastq.gz	path/to/r2.fastq.gz
id2	path/to/reads.fastq.gz
```

The outputs are :
- a pangenome in GFA format
- a genotyping output in VCF v4.2 format
- aligned reads statistics in a `*_stats.txt` file
- a `*_node_translation.tsv` containing the match between GFA node IDs and VCF node IDs.

:warning: **VCF node IDs in the ID and INFO field are different from GFA node IDs due to the vg algorithm. Use the translation table if you want to retrieve VCF nodes in GFA.**

## How to run
### Setup
In the `workflow` folder, set up the workflow in `config_graph.yaml`.
- `graphName` refers to the name you want for your graph and output directory. The output will be in `workflow_results/{graphName}`.
- `seq` is the path to to the tab-separated file of all genome used to construct the pangenome in the input.
- `ref` is the name of the genome you want to use as reference for pangenome building. Use the name you put in the `seq` file.

Still in the `workflow` folder, set up genotyping in `config_genotyping.yaml`.
- the `options` field contains `-s` and `-Q` parameters for genotyping.
- `sample` should be the path to the **(sample.tsv)** file created in input

If you use a HPC, in the folder `snakemake_profile/slurm`, set up the file `cluster_config.yml` (think to set your email address).


### Run
Create slurm_logs directory and work directory for Minigraph-Cactus.
```
mkdir -p slurm_logs
mkdir mc_temp_workdir
```
On SLURM cluster, run `sbatch job.sh dry` for a dry run or `sbatch job.sh` directly (**WARNING**: set the correct email address if you want to receive the slurm emails). Adjust the `SNG_BIND` variable as necessary if files are not found.

### If Minigraph-Cactus fails
Delete the `*_jobstore` folder before running the workflow again.

## Versions
minigraph-cactus: v2.5.2

vg: v1.53.0
